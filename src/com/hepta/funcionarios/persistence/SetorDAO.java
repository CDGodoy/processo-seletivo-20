package com.hepta.funcionarios.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.hepta.funcionarios.entity.Setor;

public class SetorDAO {

	public void salvar(Setor setor) throws Exception{
		
		EntityManager em = HibernateUtil.getEntityManager();
		
		try {
			em.getTransaction().begin();
			em.persist(setor);
			em.getTransaction().commit();
		} catch(Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
	
	public Setor atualizar(Setor setor) throws Exception{
		
		EntityManager em = HibernateUtil.getEntityManager();
		Setor novoSetor = null;
		try {
			em.getTransaction().begin();
			novoSetor = em.merge(setor);
			em.getTransaction().commit();
		} catch(Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return novoSetor;
	}
	
	public void deletar(Integer id) throws Exception{

		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			Setor setor = em.find(Setor.class, id);
			em.remove(setor);
			em.getTransaction().commit();
		}catch(Exception e) {
			em.getTransaction().rollback();
		}finally {
			em.close();
		}

	}
	
	public List<Setor> obterTodos() throws Exception{
		
		EntityManager em = HibernateUtil.getEntityManager();
		List<Setor> setores = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Setor");
			setores = query.getResultList();
		}catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		}finally {
			em.close();
		}
		return setores;
	}
}
